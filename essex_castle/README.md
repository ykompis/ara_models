# Essex Castle

Source: [Essex Castle, Alderney](https://sketchfab.com/3d-models/fire-training-center-rawscan-5b809c761a694196b1bc9ecb61d73f21)

![Preview Image](snapshot.png)

"Essex Castle, Alderney" by Archéomatique is licensed under Creative Commons Attribution. https://skfb.ly/66uDt To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
