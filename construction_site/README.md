# Construction Site

Source: [Construction Site .::RAWscan::.](https://sketchfab.com/3d-models/construction-site-rawscan-963a8cd7da8b4761ae6d0b3ca842e027)

![Preview Image](snapshot.png)

"Construction Site .::RAWscan::." by Spogna is licensed under Creative Commons Attribution-NonCommercial. https://skfb.ly/6WxoT To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
