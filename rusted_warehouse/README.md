# Rusted Warehouse

Source: [Rusted Old Warehouse .::RAWscan::.](https://sketchfab.com/3d-models/rusted-old-warehouse-rawscan-bbd177f7974d4a99a441d0c5ed42ecfa)

![Preview Image](snapshot.png)

"Rusted Old Warehouse  .::RAWscan::." by Spogna is licensed under Creative Commons Attribution. https://skfb.ly/6UnDv To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
