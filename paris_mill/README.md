# Paris Mill

Source: [Paris Mill Alma, Colorado](https://sketchfab.com/3d-models/paris-mill-alma-colorado-92c4f17087bd431eb369869578028eab)

![Preview Image](snapshot.png)

"Paris Mill Alma, Colorado" by Thomas.Elliott is licensed under Creative Commons Attribution. https://skfb.ly/6Vwo7 To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
