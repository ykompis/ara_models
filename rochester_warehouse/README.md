# Rochester Warehouse

Source: [Rochester Warehouse](https://sketchfab.com/3d-models/rochester-warehouse-85f311a17b2b4ef39f79aab4adc97e5d)

![Preview Image](snapshot.png)

"Rochester Warehouse" by MCWB Architects is licensed under Creative Commons Attribution. https://skfb.ly/6QXVv To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
