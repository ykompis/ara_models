# Baxall HQ

Source: [Baxall HQ before upgrade](https://sketchfab.com/3d-models/baxall-hq-before-upgrade-9ffa3a8577f14f1abb55ccee3e9b2d93e)

![Preview Image](snapshot.png)

"Baxall HQ before upgrade" by Blayne Jackson is licensed under Creative Commons Attribution. https://skfb.ly/69oPN To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
