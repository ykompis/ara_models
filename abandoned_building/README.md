# Abandoned Building

Source: [Scanned Abandoned Building Test](https://sketchfab.com/3d-models/scanned-abandoned-building-test-159d5ed4f7d9453885ef5eae5ebcfc6a)

![Preview Image](snapshot.png)

"Scanned Abandoned Building Test" by NNihad is licensed under Creative Commons Attribution. https://skfb.ly/XSHH To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
