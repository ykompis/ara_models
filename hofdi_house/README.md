# Hofdi House

Source: [Hofdi House - Iceland](https://sketchfab.com/3d-models/house-garden-649edf2f901e484085de564147a20bed)

![Preview Image](snapshot.png)

"Hofdi House - Iceland" by Azad Balabanian is licensed under Creative Commons Attribution. https://skfb.ly/68pZq To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
