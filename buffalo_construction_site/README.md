# Buffalo Construction Site

Source: [Buffalo Construction Site -RAWscan](https://sketchfab.com/3d-models/buffalo-construction-site-rawscan-2c28c51902314f6d9c0a6695ea54c2de)

![Preview Image](snapshot.png)

"Buffalo Construction Site -RAWscan" by Spogna is licensed under Creative Commons Attribution-NonCommercial. https://skfb.ly/6TuJ8 To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
