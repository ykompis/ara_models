# White Building

Source: [Unknown White Building .::RAWscan::.](https://sketchfab.com/3d-models/unknown-white-building-rawscan-a04a9a2e566249b59655362a5a1a650c)

![Preview Image](snapshot.png)

"Unknown White Building .::RAWscan::." by Spogna is licensed under Creative Commons Attribution. https://skfb.ly/6UG8y To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
