# Heritage library

Source: [Heritage library in Gliwice - 3D textured mesh](https://sketchfab.com/3d-models/heritage-library-in-gliwice-3d-textured-mesh-19838d2f0ce5459ab57786c49a2ed452)

![Preview Image](snapshot.png)

"Heritage library in Gliwice - 3D textured mesh" by poiformat is licensed under Creative Commons Attribution. https://skfb.ly/UvoS To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
