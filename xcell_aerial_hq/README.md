# XCell Aerial HQ

Source: [Building test Phantom 4 Pro - 20mp cam](https://sketchfab.com/3d-models/building-test-phantom-4-pro-20mp-cam-372e3c847ee74c0bb3942e22a3d0a6b8)

![Preview Image](snapshot.png)

"Building test Phantom 4 Pro - 20mp cam" by Blayne Jackson is licensed under Creative Commons Attribution. https://skfb.ly/WZwC To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
