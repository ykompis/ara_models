# Fraser Gunnery Range

Source: [Fraser Gunnery Range -RAWscan](https://sketchfab.com/3d-models/fraser-gunnery-range-rawscan-d47fa5adf60b48bba10ebec321450f5a)

![Preview Image](snapshot.png)

"Fraser Gunnery Range -RAWscan" by Spogna is licensed under Creative Commons Attribution-NonCommercial. https://skfb.ly/6TAJ6 To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
