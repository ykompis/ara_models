# Church

Source: [Model 001](https://sketchfab.com/3d-models/model-001-2f9d6c11606842aa8694214c851b2c72)

![Preview Image](snapshot.png)

"Model 001" by Warreng6465 is licensed under Creative Commons Attribution. https://skfb.ly/Ytp8 To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
