# Fire Training Center

Source: [Fire Training Center -RAWscan](https://sketchfab.com/3d-models/fire-training-center-rawscan-5b809c761a694196b1bc9ecb61d73f21)

![Preview Image](snapshot.png)

"Fire Training Center -RAWscan" by Spogna is licensed under Creative Commons Attribution-NonCommercial. https://skfb.ly/6GUDS To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
