# Sherwood Road

Source: [10700 SW Tualatin-Sherwood Road 3D Model
](https://sketchfab.com/3d-models/10700-sw-tualatin-sherwood-road-3d-model-4c28b47848ee48e3ad6c7a5501c358d6)

![Preview Image](snapshot.png)

"10700 SW Tualatin-Sherwood Road 3D Model" by TLT Photography - Aerial Mapping is licensed under Creative Commons Attribution. https://skfb.ly/6tstv To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
