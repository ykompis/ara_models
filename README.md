# Ara Models

3D Models

## Importing, scaling and removing loose parts

Example on how to import and scale a 3D model from sketchfab: [Import and Scale 3D model in Blender](https://youtu.be/GeUzN48vQhU)

(In addition to everything mentioned in the video, I suggest moving the model upwards, such that none of the mesh is below zero in the z direction. Just to avoid collisions with the ground plane in the default gazebo world. To do this select the mesh, press `G` to grab, then press `Z` to lock the movement to the z Axis, move the mesh upwards and confirm with a left mouse click.)

## How to bake multiple textures into a single texture

Based on this tutorial in an old blender version: [Baking Multiple Textures onto One Map | No Plugins](https://www.youtube.com/watch?v=9airvjDaVh4), but it also works in newer versions.
But for these large 3D scanned scenes we cannot simply unwrap the combined mesh, since this would take hours to compute. Here's how to do it without unwrapping from scratch: [Texture Baking](https://youtu.be/na7zTYCE7as).

**Correction**: for exporting the .obj choose `Y Forward` and `Z Up`. Otherwise the coordinates in blender do not correspond to the same axes in the vulkan renderer. This makes life a lot easier when figuring out bounding boxes etc.

I found a better and simpler way to rearrange the UV maps: [Easier UV remapping](https://youtu.be/fWmy0lu8EYQ).
In addition of what's in the video: you don't need to move the the UV Maps with your mouse. The width and height of every texture is exactly one unit in blender. After selecting all the UVs from a texture just grab by pressing `G`, then press `x` or `y` to move along an axis and then enter e.g. `1` to move the selected UVs exactly by one unit. No overlaps, no wasted UV space. Press `enter` to confirm and move along the other axis if necessary. This makes it super easy to arrange the UVs in a 2x2 or 3x3 grid. Then select the origin or 3D cursor in the pivot point menu and scale by `0.5` or `0.33`, respectively.


On my laptop the baking itself took around 10 to 20 minutes, but this really depends on the scene and the computer. If you have an nvidia GPU you should be able to use the GPU instead of the CPU which might be faster.
