# Skyscrapers

Source: [Skyscrapers .::RAWscan::.](https://sketchfab.com/3d-models/rochester-warehouse-85f311a17b2b4ef39f79aab4adc97e5d)

![Preview Image](snapshot.png)

"Skyscrapers .::RAWscan::." by Spogna is licensed under Creative Commons Attribution-NonCommercial. https://skfb.ly/6WPU7 To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
