# House Garden

Source: [House garden](https://sketchfab.com/3d-models/house-garden-649edf2f901e484085de564147a20bed)

![Preview Image](snapshot.png)

"House garden" by Nikitos & 3130 is licensed under Creative Commons Attribution. https://skfb.ly/6WUuz To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
