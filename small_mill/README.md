# Small Mill

Source: [Epiphany The Small Mill](https://sketchfab.com/3d-models/epiphany-the-small-mill-a82cf95935884167bc865dbfdde635d6)

![Preview Image](snapshot.png)

"Epiphany The Small Mill" by aerialpilot is licensed under Creative Commons Attribution. https://skfb.ly/6sy7t To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
