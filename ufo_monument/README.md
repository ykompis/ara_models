# UFO Monument

Source: [Buzludzha Ufo Monument -RAWscan](https://sketchfab.com/3d-models/buzludzha-ufo-monument-rawscan-7dc599fe720f4a90b2a755df915d7816)

![Preview Image](snapshot.png)

"Buzludzha Ufo Monument -RAWscan" by Spogna is licensed under Creative Commons Attribution-NonCommercial. https://skfb.ly/6XJCz To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
