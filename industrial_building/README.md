# Industrial Building

Files are compressed due to file size.

Source: [Industrial Building Model Acquired by Drone
](https://sketchfab.com/3d-models/industrial-building-model-acquired-by-drone-0e93ab7b05944087b4a19fb7262877fa)

![Preview Image](snapshot.png)

"Industrial Building Model Acquired by Drone" by TLT Photography - Aerial Mapping is licensed under Creative Commons Attribution. https://skfb.ly/68DBH To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
