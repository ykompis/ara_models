# Lafarge Factory

Source: [Site Lafarge Ouest](https://sketchfab.com/3d-models/site-lafarge-ouest-d2edb507fa854c4ab30d8fc3af17be4a)

![Preview Image](snapshot.png)

"Site Lafarge Ouest" by dzek_films is licensed under Creative Commons Attribution. https://skfb.ly/ITVK To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
