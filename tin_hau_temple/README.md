# Tin Hau Temple

Source: [Tin Hau Temple](https://sketchfab.com/3d-models/tin-hau-temple-151f2b5e2b764b5cbe2f6ad57c68d691)

![Preview Image](snapshot.png)

"Tin Hau Temple" by peter93 is licensed under Creative Commons Attribution. https://skfb.ly/6SuMx To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
